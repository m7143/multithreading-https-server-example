package org.example.app.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.UsersRegisterRQ;
import org.example.app.dto.UsersRegisterRS;
import org.example.app.exceptions.LoginRegisteredException;
import org.example.framework.auth.AuthenticationToken;
import org.example.framework.auth.Authenticator;
import org.example.framework.auth.LoginPasswordAuthenticationToken;
import org.example.framework.exception.UnsupportAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class UserManager implements Authenticator {
    private final PasswordEncoder encoder;
    private final Map<String, String> users = new HashMap<>();

    @Override
    public boolean authenticate(final AuthenticationToken request) {
        if (!(request instanceof LoginPasswordAuthenticationToken)) {
            throw new UnsupportAuthenticationToken();
        }

        final LoginPasswordAuthenticationToken converted = (LoginPasswordAuthenticationToken) request;
        final String login = converted.getLogin();
        final String password = (String) converted.getCredentials();

        final String encodedPassword;
        synchronized (this) {
            if (!users.containsKey(login)) {
                return false;
            }

            encodedPassword = users.get(login);
        }
        return encoder.matches(password, encodedPassword);
    }

    public UsersRegisterRS create(final UsersRegisterRQ requestDTO) {
        final String login = requestDTO.getLogin().trim().toLowerCase();
        final String encodedPassword = encoder.encode(requestDTO.getPassword());

        synchronized (this) {
            if (users.containsKey(login)) {
                log.error("registration with same login twice: {}", login);
                throw new LoginRegisteredException();
            }
            users.put(login, encodedPassword);

            return new UsersRegisterRS(login);
        }
    }


}
