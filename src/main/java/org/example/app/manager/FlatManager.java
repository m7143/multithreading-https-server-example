package org.example.app.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.app.domain.Flat;
import org.example.app.dto.FlatRQ;
import org.example.app.dto.FlatRS;
import org.example.app.exceptions.FlatNotFoundException;
import org.example.app.exceptions.UnauthorizedException;
import org.example.framework.auth.principal.AnonymousPrincipal;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class FlatManager {
    private final List<Flat> items = new ArrayList<>();
    private long nextId = 1;

    public int getCount() {
        return items.size();
    }

    private int getIndexById(final long id) {
        for (int i = 0; i < items.size(); i++) {
            final Flat apartment = items.get(i);
            if (apartment.getId() == id) {
                log.debug("return index: {} by id: {}", i, id);
                return i;
            }
        }
        log.debug("index by id: {} not found", id);
        return -1;
    }

    public Flat getById(final long id) throws FlatNotFoundException{
        for (final Flat item : items) {
            if (item.getId() == id) {
                log.debug("return item: {}", item);
                return item;
            }
        }
        log.debug("item by id: {} not found", id);
        throw new FlatNotFoundException(id);
    }

    public List<FlatRS> getAll() {
        return items.stream()
                .map(FlatRS::new)
                .collect(Collectors.toList());
    }

    public FlatRS create(final FlatRQ createRQ, final Principal principal) throws UnauthorizedException {
        if (principal.getName().equals(AnonymousPrincipal.ANONYMOUS)) {
            throw new UnauthorizedException("user " + principal.getName() + " not authorized for operation");
        }
        final Flat item = new Flat(nextId, principal.getName(), createRQ.getRooms(), createRQ.getPrice(),
                createRQ.getSquare(), createRQ.isBalcony(), createRQ.isLoggia(), createRQ.getFloor(),
                createRQ.getFloorInTheHouse()
        );
        log.debug("create item flat: {}", item);
        items.add(item);
        nextId += 1;
        return new FlatRS(item);
    }

    public void removeById(final long id, final Principal principal) throws UnauthorizedException, FlatNotFoundException {
        if (!getById(id).getOwner().equals(principal.getName())) {
            throw new UnauthorizedException("user " + principal.getName() + " not authorized for operation");
        }
        final boolean removed = items.removeIf(o -> o.getId() == id);
        if (!removed) {
            log.debug("remove item by id {} failed", id);
            throw new FlatNotFoundException(id);
        }
        log.debug("remove item by id {} success", id);
    }

    public FlatRS update(final FlatRQ updateRQ, final long id, final Principal principal)
            throws FlatNotFoundException, UnauthorizedException {
        final Flat item = getById(id);
        final Flat updatedItem = new Flat(id, principal.getName(), updateRQ.getRooms(), updateRQ.getPrice(),
                updateRQ.getSquare(), updateRQ.isBalcony(), updateRQ.isLoggia(), updateRQ.getFloor(),
                updateRQ.getFloorInTheHouse()
        );
        log.debug("update item id {}", id);
        items.add(updatedItem);
        return new FlatRS(updatedItem);
    }
}
