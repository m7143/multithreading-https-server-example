package org.example.app;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.handler.FlatHandler;
import org.example.app.handler.UserHandler;
import org.example.app.manager.FlatManager;
import org.example.app.manager.UserManager;
import org.example.framework.http.HttpMethods;
import org.example.framework.http.Server;
import org.example.framework.middleware.AnonAuthMiddleware;
import org.example.framework.middleware.BasicAuthNMiddleware;
import org.example.framework.util.Maps;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.io.IOException;
import java.util.regex.Pattern;


@Slf4j
public class Main {
    public static void main(String[] args) {

        final Argon2PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "1234");

        final int port = 8080;
        final Gson json = new Gson();
        final FlatManager manager = new FlatManager();
        final FlatHandler flatHandler = new FlatHandler(json, manager);
        final UserManager userManager = new UserManager(passwordEncoder);
        final UserHandler userHandler = new UserHandler(json, userManager);



        final String flatId = "(?<flatId>[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})";
        final String getById = "^/flat/"+flatId+"$";
        final String update = "^/flat/"+flatId+"/update$";
        final String delete = "^/flat/"+flatId+"/delete$";

        final Server server = Server.builder()
                .middleware(new BasicAuthNMiddleware(userManager))
                .middleware(new AnonAuthMiddleware())
                .routes(
                        Maps.of(
                                Pattern.compile("^/users$"), Maps.of(
                                        HttpMethods.POST, userHandler::register
                                ),
                                Pattern.compile("^/flats$"), Maps.of(
                                        HttpMethods.GET, flatHandler::getAll,
                                        HttpMethods.POST, flatHandler::create
                                ),
                                Pattern.compile(update), Maps.of(
                                        HttpMethods.POST, flatHandler::update
                                ),
                                Pattern.compile(getById), Maps.of(
                                        HttpMethods.GET, flatHandler::getById
                                ),
                                Pattern.compile(delete), Maps.of(
                                        HttpMethods.POST, flatHandler::delete
                                )
                        )
                )
                .build();

        try {
            server.serveHTTPS(port);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
