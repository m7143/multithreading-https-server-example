package org.example.app.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class FlatRQ {
    private String rooms;
    private int price;
    private int square;
    private boolean balcony;
    private boolean loggia;
    private int floor;
    private int floorInTheHouse;
}
