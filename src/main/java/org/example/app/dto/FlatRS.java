package org.example.app.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.app.domain.Flat;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class FlatRS {
    private long id;
    private String rooms;
    private int price;
    private int square;
    private boolean balcony;
    private boolean loggia;
    private int floor;
    private int floorInTheHouse;

    public FlatRS(Flat flat) {
        this.id = id;
        this.rooms = rooms;
        this.price = price;
        this.square = square;
        this.balcony = balcony;
        this.loggia = loggia;
        this.floor = floor;
        this.floorInTheHouse = floorInTheHouse;
    }
}
