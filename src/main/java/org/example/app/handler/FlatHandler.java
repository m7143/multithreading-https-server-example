package org.example.app.handler;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.FlatRQ;
import org.example.app.dto.FlatRS;
import org.example.app.exceptions.FlatNotFoundException;
import org.example.app.exceptions.UnauthorizedException;
import org.example.app.manager.FlatManager;
import org.example.framework.http.Request;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class FlatHandler {
    private static final String OK = "200 Ok";
    private static final String BAD_REQUEST = "400 Bad Request";
    private static final String USER_NOT_AUTHORIZED_BODY = "{message: user not authorized}";
    private static final String FLAT_NOT_FOUND_BODY = "{message: not found}";

    private final Gson json;
    private final FlatManager flatManager;

    public void create(final Request request, final OutputStream responseStream) throws IOException {
        try {
            Principal principal = request.getPrincipal();
            String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
            FlatRQ createRQ = json.fromJson(requestBody, FlatRQ.class);
            FlatRS createRS = flatManager.create(createRQ, principal);
            final byte[] responseBody = json.toJson(createRS).getBytes(StandardCharsets.UTF_8);
            writeResponse(responseStream, OK, responseBody);
        }
        catch (UnauthorizedException e) {
            writeResponse(responseStream, BAD_REQUEST, USER_NOT_AUTHORIZED_BODY.getBytes(StandardCharsets.UTF_8));
        }
    }

    private void writeResponse(OutputStream responseStream, String status, byte[] body) throws IOException {
        responseStream.write((
                "HTTP/1.1 " + status + "\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: application/json\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }

    public void getAll(final Request request, final OutputStream responseStream) throws IOException {
        final List<FlatRS> responseDTO = flatManager.getAll();
        final byte[] responseBody = json.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, OK, responseBody);
    }

    public void getById(final Request request, final OutputStream responseStream) throws IOException {
        try {
            final int id = Integer.parseInt(request.getPathGroup("FlatId"));
            final byte[] responseBody = json.toJson(flatManager.getById(id)).getBytes(StandardCharsets.UTF_8);
            writeResponse(responseStream, OK, responseBody); }
        catch (FlatNotFoundException e) {
            writeResponse(responseStream, BAD_REQUEST, FLAT_NOT_FOUND_BODY.getBytes(StandardCharsets.UTF_8));
        }
    }

    public void update(final Request request, final OutputStream responseStream) throws IOException {
        try {
            final Principal principal = request.getPrincipal();
            final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
            final int id = Integer.parseInt(request.getPathGroup("FlatId"));
            final FlatRQ updateRQ = json.fromJson(requestBody, FlatRQ.class);
            final FlatRS updateRS = flatManager.update(updateRQ, id, principal);
            final byte[] responseBody = json.toJson(updateRS).getBytes(StandardCharsets.UTF_8);
            writeResponse(responseStream, OK, responseBody);
        }
        catch (UnauthorizedException e) {
            writeResponse(responseStream, BAD_REQUEST, USER_NOT_AUTHORIZED_BODY.getBytes(StandardCharsets.UTF_8));
        }
        catch (FlatNotFoundException e) {
            writeResponse(responseStream, BAD_REQUEST, FLAT_NOT_FOUND_BODY.getBytes(StandardCharsets.UTF_8));
        }
    }

    public void delete(final Request request, final OutputStream responseStream) throws IOException {
        try {
            final Principal principal = request.getPrincipal();
            final int id = Integer.parseInt(request.getPathGroup("flatId"));
            flatManager.removeById(id, principal);
            writeResponse(responseStream, OK, ("{ message: remove item by id " + id + " remooved}").getBytes(StandardCharsets.UTF_8)); }
        catch (UnauthorizedException e) {
            writeResponse(responseStream, BAD_REQUEST, USER_NOT_AUTHORIZED_BODY.getBytes(StandardCharsets.UTF_8));
        }
        catch (FlatNotFoundException e) {
            writeResponse(responseStream, BAD_REQUEST, FLAT_NOT_FOUND_BODY.getBytes(StandardCharsets.UTF_8));
        }
    }
}
