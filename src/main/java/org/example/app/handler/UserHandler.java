package org.example.app.handler;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.UsersRegisterRQ;
import org.example.app.dto.UsersRegisterRS;
import org.example.app.manager.UserManager;
import org.example.framework.http.Request;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
@Slf4j
@RequiredArgsConstructor
public class UserHandler {
    private final Gson gson;
    private final UserManager manager;

    public void register(final Request request, final OutputStream responseStream) throws IOException {
        final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
        final UsersRegisterRQ requestDTO = gson.fromJson(requestBody, UsersRegisterRQ.class);

        final UsersRegisterRS responseDTO = manager.create(requestDTO);
        final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    private void writeResponse(OutputStream responseStream, byte[] body) throws IOException {
        responseStream.write((
                "HTTP/1.1 200 Ok\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: application/json\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }
}
