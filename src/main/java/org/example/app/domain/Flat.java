package org.example.app.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Flat {
    private long id;
    private String owner;
    private String rooms;
    private int price;
    private int square;
    private boolean balcony;
    private boolean loggia;
    private int floor;
    private int floorInTheHouse;
}
